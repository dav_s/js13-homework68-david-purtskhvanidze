import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MsgService } from './shared/msg.service';
import { Msg } from './shared/msg.model';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import { HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('f') msgForm!: NgForm;

  messages: Msg[] = [];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private msgService: MsgService) { }

  ngOnInit() {
    this.messagesChangeSubscription = this.msgService.messagesChange.subscribe((msg: Msg[]) => {
      this.messages = msg;
    });

    this.messagesFetchingSubscription = this.msgService.messagesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });

    this.msgService.start();
  }

  ngOnDestroy() {
    this.msgService.stop();

    this.messagesChangeSubscription.unsubscribe();
    this.messagesFetchingSubscription.unsubscribe();
  }

  postMsg() {
    const body = new HttpParams()
      .set('author', this.msgForm.value.author)
      .set('message', this.msgForm.value.message);

    this.msgService.post(body);
  }
}
