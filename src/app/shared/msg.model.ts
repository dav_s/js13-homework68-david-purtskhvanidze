export class Msg {
  constructor(
    public author: string,
    public datetime: string,
    public message: string,
    public id: string,
  ) {}
}
